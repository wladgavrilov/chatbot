**What is this**

This is toy chat with GPT

**How to run**

Put your `OPENAI_API_KEY=` to `.env`

Install requirements

```pip3 install -r requirements.txt```

Run the server

```python3 main.py```

