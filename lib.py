import hashlib
import pickle


def hash(message: str) -> str:
    m = hashlib.sha256()
    m.update(message.encode("utf-8"))
    return m.hexdigest()


def dumps(message):
    return pickle.dumps(message).decode("latin1")


def loads(message):
    return pickle.loads(message.encode("latin1"))
