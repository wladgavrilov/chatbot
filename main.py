import os
from load_dotenv import load_dotenv

from app import app


load_dotenv()


@app.route("/hallo")
def hello_world():
    return "Hallo Alle"


if __name__ == "__main__":
    isDebug = int(os.environ.get("DEBUG", "0"))
    app.run(debug=isDebug, host="0.0.0.0", port=int(os.environ.get("PORT", 8080)))
