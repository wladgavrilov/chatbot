import os
import json
import time
import pickle

from load_dotenv import load_dotenv
from flask import Flask, render_template, jsonify, request

import chromadb
from chromadb.api.models import Collection
from chromadb.config import Settings

from langchain.chat_models import ChatOpenAI
from langchain.chat_models.base import HumanMessage, AIMessage

from lib import hash, dumps, loads


load_dotenv()

COLLECTION_NAME = os.environ.get("COLLECTION_NAME", "my_collection")
chroma = chromadb.Client(
    Settings(
        chroma_db_impl="duckdb+parquet",
        persist_directory="./db",
    )
)

collection = chroma.get_or_create_collection(name=COLLECTION_NAME)


app = Flask(__name__, static_folder="static/")


class Database:
    def __init__(self, collection: chromadb.api.models.Collection):
        self.collection = collection

    def save(self, message: HumanMessage or AIMessage) -> str:
        timestamp = time.time()
        serialized = dumps(message)
        id = hash("{} {}".format(timestamp, serialized))
        author = "ai" if type(message) is AIMessage else "human"

        self.collection.upsert(
            metadatas=[
                {"timestamp": str(timestamp), "author": author, "type": "message"}
            ],
            documents=[serialized],
            ids=[id],
        )

        return id

    def get_all(self):
        response = self.collection.get(where={"type": "message"})

        messages = []
        print(response["ids"])
        for i in range(0, len(response.get("ids", []))):
            document = response["documents"][i]
            metadata = response["metadatas"][i]
            message = loads(document)
            messages.append(message)
        return messages


DB = Database(collection)
bot = ChatOpenAI(model_name="gpt-3.5-turbo")


@app.route("/get_all")
def get_all():
    messages = DB.get_all()
    return jsonify(
        [
            {
                "content": msg.content,
                "author": "ai" if type(msg).__name__ == "AIMessage" else "human",
            }
            for msg in messages
        ]
    )


@app.route("/send", methods=["GET"])
def send():
    message = request.args.get("message")
    message = HumanMessage(content=message)
    DB.save(message)
    messages = DB.get_all()
    response = bot.generate(messages=[messages])
    response = response.generations.pop().pop().message

    DB.save(response)
    return response.content


@app.route("/clear")
def clear():
    chroma.reset()
    chroma.get_or_create_collection(name=COLLECTION_NAME)
    return "", 200


@app.route("/")
def index():
    return render_template("index.html")
